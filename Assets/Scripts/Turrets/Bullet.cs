﻿using UnityEngine;

public class Bullet : MonoBehaviour
{

    private Transform target;
    public GameObject impactEffect;
    public float explosionRadius = 0f;
    private EnemyMovement enemyStats;

    public float speed = 70f;
    public float damageDealt;
    public void Seek(Transform _target)
    {
        target = _target;
    }
    void Start()
    {

    }
    // Update is called once per frame
    void Update()
    {
        if (target == null)
        {
            Destroy(gameObject);
            return;
        }

        Vector3 dir = target.position - transform.position;
        float distanceThisFrame = speed * Time.deltaTime;

        if (dir.magnitude <= distanceThisFrame)
        {
            HitTarget();
            return;
        }

        transform.Translate(dir.normalized * distanceThisFrame, Space.World);
        transform.LookAt(target);

    }
    void HitTarget()
    {

        Damage(target);
        Destroy(gameObject);

    }

    /* void Explode()
     {
         Collider[] colliders = Physics.OverlapSphere(transform.position, explosionRadius);
         foreach (Collider collider in colliders)
         {
             Debug.Log("works :)");
             if (collider.tag == "Enemy")
             {
                 Damage(collider.transform);
             }
         }
     }*/

    void Damage(Transform enemy)
    {
        GameObject effectInts = (GameObject)Instantiate(impactEffect, transform.position, transform.rotation);
        Destroy(effectInts, 5f);
    }


    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, explosionRadius);
    }
}

