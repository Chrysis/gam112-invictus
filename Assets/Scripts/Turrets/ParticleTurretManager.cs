﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleTurretManager : MonoBehaviour {

    private Transform target;

    [Header("Attributes")]

    public float range = 15f;
    public float fireSpeed = 1f;
    //public float fireRate = 1f;
    private float fireCountdown = 0f;

    [Header("Unity Setup")]

    public string enemyTag = "Enemy";
    public Transform partToRotate;
    public float turnSpeed = 10f;
    public GameObject bulletPrefab;
    public Transform firePoint;
    public ParticleSystem particleEffect;

    public ParticleSystem part;
    public List<ParticleCollisionEvent> collisionEvents;
    // Use this for initialization
    void Start()
    {
        // fireRate = 1f;
        InvokeRepeating("UpdateTarget", 0f, 0.5f);
        particleEffect.enableEmission = false;

    }

    void UpdateTarget()
    {
        GameObject[] enemies = GameObject.FindGameObjectsWithTag(enemyTag);
        float shortestDistance = Mathf.Infinity;
        GameObject nearestEnemy = null;
        foreach (GameObject enemy in enemies)
        {
            float distanceToEnemy = Vector3.Distance(transform.position, enemy.transform.position);
            if (distanceToEnemy < shortestDistance)
            {
                shortestDistance = distanceToEnemy;
                nearestEnemy = enemy;
            }
        }

        if (nearestEnemy != null && shortestDistance <= range)
        {
            Shoot();
            target = nearestEnemy.transform;
        }
        else
        {
            particleEffect.enableEmission = false;

            target = null;
        }

    }

    void Update()
    {
        if (target == null)
        {
            return;
        }
        //Target lock-on.

        Vector3 dir = target.transform.position - transform.position;
        Quaternion lookRotation = Quaternion.LookRotation(dir);
        Vector3 rotation = Quaternion.Lerp(partToRotate.rotation, lookRotation, Time.deltaTime * turnSpeed).eulerAngles;
        partToRotate.rotation = Quaternion.Euler(0f, rotation.y, 0f);

        if (fireCountdown <= 0f)
        {
            fireCountdown = 1f / fireSpeed;
        }

        fireCountdown -= Time.deltaTime;

    }

    void Shoot()
    {
       particleEffect.enableEmission = true;

    }
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, range);
    }
        
}

