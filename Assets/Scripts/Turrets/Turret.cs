﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Turret : MonoBehaviour {

    private Transform target;

    [Header ("Attributes")]

    public float range = 15f;
    public float fireSpeed = 1f;
  //public float fireRate = 1f;
    private float fireCountdown = 0f;

    [Header ("Unity Setup")]

    public string enemyTag = "Enemy";
    public Transform partToRotate;
    public float turnSpeed = 10f;
    public GameObject bulletPrefab;
    public Transform firePoint;

    private Audio audioPlay;




	// Use this for initialization
	void Start () {
       // fireRate = 1f;
        InvokeRepeating("UpdateTarget", 0f, 0.5f);
        audioPlay = FindObjectOfType<Audio>();
    }
	
    void UpdateTarget ()
    {
        GameObject[] enemies = GameObject.FindGameObjectsWithTag(enemyTag);
        GameObject[] prioritiseEnemies = GameObject.FindGameObjectsWithTag("EnemyPrioritise");
        float shortestDistance = Mathf.Infinity;
        float shortestPriorDistance = Mathf.Infinity;
        GameObject nearestEnemy = null;
        GameObject nearestPriorEnemy = null;

        foreach (GameObject priorEnemy in prioritiseEnemies)
        {
            float distanceToPriorEnemy = Vector3.Distance(transform.position, priorEnemy.transform.position);

            if (distanceToPriorEnemy < shortestDistance)
            {
                shortestPriorDistance = distanceToPriorEnemy;
                nearestPriorEnemy = priorEnemy;
            }
        }
        foreach (GameObject enemy in enemies)
        {
            float distanceToEnemy = Vector3.Distance(transform.position, enemy.transform.position);


            if (distanceToEnemy < shortestDistance)
            {
                shortestDistance = distanceToEnemy;
                nearestEnemy = enemy;
            }
        }
        if(nearestPriorEnemy != null && shortestPriorDistance <= range)
        {
            target = nearestPriorEnemy.transform;

        }else if (nearestEnemy != null && shortestDistance <= range)
        {
            target = nearestEnemy.transform;
        }
        else
        {
            target = null;
        }

    }

	// Update is called once per frame
	void Update () {
        if (target == null)
        {
            return;
        }
        //Target lock-on.
        Vector3 dir = target.transform.position - transform.position;
        Quaternion lookRotation = Quaternion.LookRotation(dir);
        Vector3 rotation = Quaternion.Lerp(partToRotate.rotation, lookRotation, Time.deltaTime * turnSpeed).eulerAngles;
        partToRotate.rotation = Quaternion.Euler (0f, rotation.y, 0f);

        if (fireCountdown <= 0f)
        {
            Shoot();
            fireCountdown = 1f / fireSpeed;
        }

        fireCountdown -= Time.deltaTime;

	}

    void Shoot ()
    {
        GameObject bulletGO = (GameObject)Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);
        Bullet bullet = bulletGO.GetComponent<Bullet>();
        audioPlay.playTurretShoot();
        if (bullet != null)
        {
            bullet.Seek(target);
        }
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, range);
    }


}
