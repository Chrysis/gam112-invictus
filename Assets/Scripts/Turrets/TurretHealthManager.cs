﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretHealthManager : MonoBehaviour
{
    public float health;
    public int cost;
    float damage = 1;

    private Audio audioPlay;
    private EnemyShootManager enemyShootStats;
    void Start ()
    {
        enemyShootStats = GetComponent<EnemyShootManager>();
        audioPlay = FindObjectOfType<Audio>();
    }
	
	void Update ()
    {
		if(health <= 0)
        {
            audioPlay.playEnemyDeath();
            Object.Destroy(gameObject,0f);

        }
	}
    void OnCollisionEnter(Collision col)
    {
        if(col.gameObject.tag == "EnemyShot")
        {
            health -= 1;

        }
        if (col.gameObject.tag == "EnemyShotDragon")
        {
            health -= 3;
        }
    }
}
