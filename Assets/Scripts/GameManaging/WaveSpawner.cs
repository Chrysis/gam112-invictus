﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class WaveSpawner : MonoBehaviour {

    public GameObject dronePrefab;
    public GameObject wolfPrefab;
    public GameObject goblinPrefab;
    public GameObject BehemothPrefab;
    public GameObject WyvernPrefab;

    public Transform spawnPoint;

    bool nextWave = false;
    int currentWaveNumber;
    public int waveNumber = 0;

    public GameObject nextWaveButton;

    public List<GameObject> Wave1 = new List<GameObject>();
    public List<GameObject> Wave2 = new List<GameObject>();
    public List<GameObject> Wave3 = new List<GameObject>();
    public List<GameObject> Wave4 = new List<GameObject>();
    public List<GameObject> Wave5 = new List<GameObject>();
    public List<GameObject> Wave6 = new List<GameObject>();
    public List<GameObject> Wave7 = new List<GameObject>();
    public List<GameObject> Wave8 = new List<GameObject>();
    public List<GameObject> Wave9 = new List<GameObject>();
    public List<GameObject> Wave10 = new List<GameObject>();

    private int waveIndex = 0;

    public int levelUnlocked = 1;

    public void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
    }
    void Start()
    {
        List<GameObject> Wave1 = new List<GameObject>();
    }
    public void Update()
    {
        if (nextWave == true)
        {

            StartCoroutine(SpawnWave());
        }
        if (nextWave == false)
        {
            currentWaveNumber = waveNumber;

        }
        waveNumber = currentWaveNumber;
    }
    IEnumerator SpawnWave ()
    {
        for (int i = 0; i < i + 1; i++)
        {

            SpawnEnemy();


            yield return new WaitForSeconds(0.5f);
        }
    }

    public void SpawnEnemy()
    {
 
        if (waveNumber == 1)
        {
            Instantiate(Wave1[0], new Vector3(spawnPoint.position.x, 3, spawnPoint.position.z), spawnPoint.rotation);
            Wave1.RemoveAt(waveIndex);
            if (Wave1.Count == 0)
            {
                nextWave = true;
                nextWaveButton.SetActive(true);

            }
            nextWave = false;
        }
        if (waveNumber == 2)
        {
            Instantiate(Wave2[0], new Vector3(spawnPoint.position.x, 3, spawnPoint.position.z), spawnPoint.rotation);
            Wave2.RemoveAt(waveIndex);
            if (Wave2.Count == 0)
            {
                nextWave = true;
                nextWaveButton.SetActive(true);

            }
            nextWave = false;
        }
        if (waveNumber == 3)
        {
            Instantiate(Wave3[0], new Vector3(spawnPoint.position.x, 3, spawnPoint.position.z), spawnPoint.rotation);
            Wave3.RemoveAt(waveIndex);
            if (Wave3.Count == 0)
            {
                nextWave = true;
                nextWaveButton.SetActive(true);

            }
            nextWave = false;
        }
        if (waveNumber == 4)
        {
            Instantiate(Wave4[0], new Vector3(spawnPoint.position.x, 3, spawnPoint.position.z), spawnPoint.rotation);
            Wave4.RemoveAt(waveIndex);
            if (Wave4.Count == 0)
            {
                nextWave = true;
                nextWaveButton.SetActive(true);

            }
            nextWave = false;
        }
        if (waveNumber == 5)
        {
            Instantiate(Wave5[0], new Vector3(spawnPoint.position.x, 3, spawnPoint.position.z), spawnPoint.rotation);
            Wave5.RemoveAt(waveIndex);
            if (Wave5.Count == 0)
            {
                nextWave = true;
                nextWaveButton.SetActive(true);

            }
            nextWave = false;

        }
        if (waveNumber == 6)
        {
            Instantiate(Wave6[0], new Vector3(spawnPoint.position.x, 3, spawnPoint.position.z), spawnPoint.rotation);
            Wave6.RemoveAt(waveIndex);
            if (Wave6.Count == 0)
            {
                nextWave = true;
                nextWaveButton.SetActive(true);

            }
            nextWave = false;

        }
        if (waveNumber == 7)
        {
            Instantiate(Wave7[0], new Vector3(spawnPoint.position.x, 3, spawnPoint.position.z), spawnPoint.rotation);
            Wave7.RemoveAt(waveIndex);
            if (Wave7.Count == 0)
            {
                nextWave = true;
                nextWaveButton.SetActive(true);

            }
            nextWave = false;

        }
        if (waveNumber == 8)
        {
            Instantiate(Wave8[0], new Vector3(spawnPoint.position.x, 3, spawnPoint.position.z), spawnPoint.rotation);
            Wave8.RemoveAt(waveIndex);
            if (Wave8.Count == 0)
            {
                nextWave = true;
                nextWaveButton.SetActive(true);

            }
            nextWave = false;

        }
        if (waveNumber == 9)
        {
            Instantiate(Wave9[0], new Vector3(spawnPoint.position.x, 3, spawnPoint.position.z), spawnPoint.rotation);
            Wave9.RemoveAt(waveIndex);
            if (Wave9.Count == 0)
            {
                nextWave = true;
                nextWaveButton.SetActive(true);

            }
            nextWave = false;

        }
        if (waveNumber == 10)
        {
            Instantiate(Wave10[0], new Vector3(spawnPoint.position.x, 3, spawnPoint.position.z), spawnPoint.rotation);
            Wave10.RemoveAt(waveIndex);
            if (Wave10.Count == 0)
            {
                nextWave = true;
                nextWaveButton.SetActive(true);

            }
            nextWave = false;

        }
        if(waveNumber >= 11)
        {
            //if(levelUnlocked == 1)
           // {
            //    levelUnlocked = 2;
                //SceneManager.LoadScene("Main_Menu");

           // }
            //if (levelUnlocked == 2)
            //{
            //    levelUnlocked = 3;
                //SceneManager.LoadScene("Main_Menu");

            //}
            //if (levelUnlocked == 3)
           // {
                //levelUnlocked++;
                SceneManager.LoadScene("Win");

            }
        }
    

    public void NextButtonPressed()
    {
        if (nextWave == false)
        {
            waveNumber += 1;
            nextWave = true;
            nextWaveButton.SetActive(false);
        }
    }
}
    
       /* {
            if (waveIndex ==1)
        {
            Instantiate(dronePrefab, new Vector3(spawnPoint.position.x, 3, spawnPoint.position.z ), spawnPoint.rotation);
        }
        if(waveIndex ==2)
        {
            Instantiate(goblinPrefab, spawnPoint.position, spawnPoint.rotation);
        }
        if (waveIndex == 3)
        {
            Instantiate(goblinPrefab, spawnPoint.position, spawnPoint.rotation);
            Instantiate(wolfPrefab, spawnPoint.position, spawnPoint.rotation);

        }*/




