﻿using UnityEngine;
using UnityEngine.EventSystems;

public class Node : MonoBehaviour {

    public Shop shop;
    BuildManager buildManager;

    public Color hoverColour;
    private Color startColour;

    public Vector3 positionOffset;
    public bool bought;
    private GameObject turret;
    public Shop costs;
    private Renderer rend;

    private void Start()
    {
        buildManager = BuildManager.instance;
        rend = GetComponent<Renderer>();
        startColour = rend.material.color;
    }
    void Update()
    {
    }
    public void OnMouseDown()
    {

        if (EventSystem.current.IsPointerOverGameObject())
        {
            return;
        }

        if (buildManager.GetTurretToBuild() == null)
        {
            return;
        }

        if (turret != null)
        {
            Debug.Log("Can't build there.");
            return;
        }

        if(buildManager.GetTurretToBuild())
        {
            if (shop.totalCurrency >= shop.placeCost)
            {
                shop.totalCurrency -= shop.placeCost;
                GameObject turretToBuild = BuildManager.instance.GetTurretToBuild();
                turret = (GameObject)Instantiate(turretToBuild, transform.position + positionOffset, Quaternion.Euler(new Vector3(0, transform.rotation.y, transform.rotation.z)));

                shop.justPlaced = false;
                Debug.Log("Here?");
            }

            return;
        }
    }

    void OnMouseEnter()
    {
        if (EventSystem.current.IsPointerOverGameObject())
        {
            return;
        }

        if (buildManager.GetTurretToBuild() == null)
        {
            return;
        }
        rend.material.color = hoverColour;  
    }

    private void OnMouseExit()
    {
        rend.material.color = startColour;
    }
}
