﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine;

public class PlayerHealth : MonoBehaviour
{
    public int playerHealth;

    public GameObject drone;
    public GameObject wolf;
    public GameObject goblin;
    public GameObject rhino;
    public GameObject dragon;

    public Text healthText;

	void Start ()
    {

    }
	
	void Update ()
    {
        healthText.text = "Health: " + playerHealth;
        if (playerHealth <= 0)
        {
            SceneManager.LoadScene("Main_Menu");
        }
	}
    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.name == "Drone(Clone)")
        {
            playerHealth -= 5;
            healthText.text = "Health: " + playerHealth;
            Destroy(other.gameObject);
        }
        if (other.gameObject.name == "DireWolf(Clone)")
        {
            playerHealth -= 15;
            healthText.text = "Health: " + playerHealth;
            Destroy(other.gameObject);
        }
        if (other.gameObject.name == "Goblin(Clone)")
        {
            playerHealth -= 10;
            healthText.text = "Health: " + playerHealth;
            Destroy(other.gameObject);
        }
        if (other.gameObject.name == "Rhino(Clone)")
        {
            playerHealth -= 80;
            healthText.text = "Health: " + playerHealth;
            Destroy(other.gameObject);
        }
        if (other.gameObject.name == "DarkDragon(Clone)")
        {
            playerHealth -= 50;
            healthText.text = "Health: " + playerHealth;
            Destroy(other.gameObject);
        }

    }
}
