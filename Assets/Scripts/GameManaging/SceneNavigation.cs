﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneNavigation : MonoBehaviour {
    public WaveSpawner waves;

    public void Start()
    {
        waves = FindObjectOfType<WaveSpawner>();
    }
    public void MainMenu()
    {
        SceneManager.LoadScene("Main_Menu");
    }

    public void Level1()
    {
        SceneManager.LoadScene("Level1");
    }

    public void Level2()
    {
        if (waves.levelUnlocked > 1)
        {
            SceneManager.LoadScene("Level2");
        }
    }

    public void Level3()
    {
        if (waves.levelUnlocked > 2)
        {
            SceneManager.LoadScene("Level3");
        }
    }
}
