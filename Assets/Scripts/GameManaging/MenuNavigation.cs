﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuNavigation : MonoBehaviour {

    public GameObject Menu_page;
    public GameObject HTP_page1;
    public GameObject HTP_page2;
    public GameObject HTP_page3;
    public GameObject Credits_page;

    public int Page;

    public void MainMenu()
    {
        SceneManager.LoadScene("Main_Menu");
        Page = 1;
    }

    public void HTP1()
    {
        Page = 2;
    }

    public void HTP2()
    {
        Page = 3;
    }

    public void HTP3()
    {
        Page = 4;
    }

    public void Credits()
    {
        Page = 5;
    }

    public void LevelSelect()
    {
        SceneManager.LoadScene("Level_Select");
    }

    public void Level1()
    {
        SceneManager.LoadScene("Level1");
    }

    public void Level2()
    {
        SceneManager.LoadScene("Level2");
    }

    public void Level3()
    {
        SceneManager.LoadScene("Level3");
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void Update()
    {
        if(Page == 1)
        {
            Menu_page.SetActive(true);
            HTP_page1.SetActive(false);
            HTP_page2.SetActive(false);
            HTP_page3.SetActive(false);
            Credits_page.SetActive(false);
        }
        else if (Page == 2)
        {
            Menu_page.SetActive(false);
            HTP_page1.SetActive(true);
            HTP_page2.SetActive(false);
            HTP_page3.SetActive(false);
            Credits_page.SetActive(false);
        }
        else if (Page == 3)
        {
            Menu_page.SetActive(false);
            HTP_page1.SetActive(false);
            HTP_page2.SetActive(true);
            HTP_page3.SetActive(false);
            Credits_page.SetActive(false);
        }
        else if (Page == 4)
        {
            Menu_page.SetActive(false);
            HTP_page1.SetActive(false);
            HTP_page2.SetActive(false);
            HTP_page3.SetActive(true);
            Credits_page.SetActive(false);
        }
        else if (Page == 5)
        {
            Menu_page.SetActive(false);
            HTP_page1.SetActive(false);
            HTP_page2.SetActive(false);
            HTP_page3.SetActive(false);
            Credits_page.SetActive(true);
        }
    }
}
