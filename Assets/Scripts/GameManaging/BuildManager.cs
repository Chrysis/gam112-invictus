﻿using UnityEngine;

public class BuildManager : MonoBehaviour {

    public static BuildManager instance;
    private Shop costs;
    private void Awake()
    {
        costs = GetComponent<Shop>();
        if (instance != null)
        {
            Debug.Log("More than one Build Manager!");
        }
        instance = this;
    }

    public GameObject standardTurretPrefab;
    public GameObject missileLauncherPrefab;
    public GameObject flameTurretPrefab;
    public GameObject railGunPrefab;
    public GameObject laserTurretPrefab;

    public GameObject empty;
    private GameObject turretToBuild;

    public GameObject GetTurretToBuild()
    {
        return turretToBuild;
    }

    public void SetTurretToBuild (GameObject turret)
    {
        turretToBuild = turret;
    }

}
