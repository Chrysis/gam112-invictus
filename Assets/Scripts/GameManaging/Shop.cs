﻿using UnityEngine;
using UnityEngine.UI;


public class Shop : MonoBehaviour {

    BuildManager buildManager;
    private TurretHealthManager turretStats;
    private Node node;

    public int totalCurrency;

    public int standardTurretCost;
    public int missileTurretCost;
    public int flameTurretCost;
    public int railGunCost;
    public int laserTurretCost;

    public int placeCost;

    public bool justPlaced = false;
    public Text PlayerCurrency;

    private void Start()
    {
        node = GetComponent<Node>();
        turretStats = GetComponent<TurretHealthManager>();
        buildManager = BuildManager.instance;
    }

    public void Update()
    {
        PlayerCurrency.text = "$" + Mathf.Round(totalCurrency).ToString();

    }
    public void PurchaseStandardTurret ()
    {
        if (totalCurrency >= standardTurretCost && justPlaced == false)
        {
            placeCost = standardTurretCost;

            Debug.Log("Standard Turret selected.");
            buildManager.SetTurretToBuild(buildManager.standardTurretPrefab);
        }else
        {
            Debug.Log("Sorry You Cant Afford This.");
        }
     
    }
    public void PurchaseMissileLauncher()
    {
        if (totalCurrency >= missileTurretCost && justPlaced == false)
        {
            placeCost = missileTurretCost;

            Debug.Log("Missile Turret selected.");
            buildManager.SetTurretToBuild(buildManager.missileLauncherPrefab);
        }
        else
        {
            Debug.Log("Sorry You Cant Afford This.");
        }
    }
    public void PurchaseFlameTurret()
    {
        if (totalCurrency >=  flameTurretCost && justPlaced == false)
        {
            placeCost = flameTurretCost;
            Debug.Log("Flame Turret selected.");
            buildManager.SetTurretToBuild(buildManager.flameTurretPrefab);
        }
        else
        {
            Debug.Log("Sorry You Cant Afford This.");
        }
    }
    public void PurchaseRailGunTurret()
    {
        if (totalCurrency >= railGunCost && justPlaced == false)
        {
            placeCost = railGunCost;

            Debug.Log("RailGun Turret selected.");
            buildManager.SetTurretToBuild(buildManager.railGunPrefab);
        }
        else
        {
            Debug.Log("Sorry You Cant Afford This.");
        }
    }
    public void PurchaseLaserTurret()
    {
        if (totalCurrency >= laserTurretCost && justPlaced == false)
        {
            placeCost = laserTurretCost;

            Debug.Log("Laser Turret selected.");
            buildManager.SetTurretToBuild(buildManager.laserTurretPrefab);
            justPlaced = true;
        }
        else
        {
            Debug.Log("Sorry You Cant Afford This.");
        }
    }  
}
