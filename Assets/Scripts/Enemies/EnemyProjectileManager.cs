﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyProjectileManager : MonoBehaviour
{

    private Transform target;
    public GameObject impactEffect;
    public float explosionRadius = 0f;

    private EnemyMovement enemyStats;
    private TurretHealthManager turretStats;
    private EnemyShootManager enemyShootStats;
    public float speed = 70f;
    public void Seek(Transform _target)
    {
        target = _target;
    }
    void Start()
    {
        turretStats = GetComponent<TurretHealthManager>();
        enemyShootStats = GetComponent<EnemyShootManager>();
    }
    // Update is called once per frame
    void Update()
    {
        if (target == null)
        {
            Destroy(gameObject);
            return;
        }

        Vector3 dir = target.position - transform.position;
        float distanceThisFrame = speed * Time.deltaTime;

        if (dir.magnitude <= distanceThisFrame)
        {
            HitTarget();
            return;
        }

        transform.Translate(dir.normalized * distanceThisFrame, Space.World);
        transform.LookAt(target);

    }
    void HitTarget()
    {
        Destroy(gameObject);
    }

}
