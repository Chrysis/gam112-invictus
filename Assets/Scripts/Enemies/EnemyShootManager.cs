﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShootManager : MonoBehaviour
{

    private Transform target;

    [Header("Attributes")]

    public float range = 15f;
    public float fireSpeed = 1f;
    private float fireCountdown = 0f;
    public float damage;
    [Header("Unity Setup")]

    public string turretTag = "Turret";
    public Transform partToRotate;
    public float turnSpeed = 10f;
    public GameObject bulletPrefab;
    public Transform firePoint;
    public ParticleSystem fire;
    public bool inRange;

    // Use this for initialization
    void Start()
    {
        // fireRate = 1f;
        InvokeRepeating("UpdateTarget", 0f, 0.5f);

    }

    void UpdateTarget()
    {
        GameObject[] turret = GameObject.FindGameObjectsWithTag(turretTag);
        float shortestDistance = Mathf.Infinity;
        GameObject nearestTurret = null;
        foreach (GameObject enemy in turret)
        {
            float distanceToEnemy = Vector3.Distance(transform.position, enemy.transform.position);
            if (distanceToEnemy < shortestDistance)
            {
                shortestDistance = distanceToEnemy;
                nearestTurret = enemy;
            }
        }

        if (nearestTurret != null && shortestDistance <= range)
        {
            target = nearestTurret.transform;
        }
        else
        {
            target = null;
        }

    }

    // Update is called once per frame
    void Update()
    {
        if (target == null)
        {
            inRange = false;
            fire.enableEmission = false;

            return;
        }
        Vector3 dir = target.transform.position - transform.position;
        Quaternion lookRotation = Quaternion.LookRotation(dir);
        Vector3 rotation = Quaternion.Lerp(partToRotate.rotation, lookRotation, Time.deltaTime * turnSpeed).eulerAngles;
        partToRotate.rotation = Quaternion.Euler(0f, rotation.y, 0f);
        inRange = true;
        fire.enableEmission = true;

        if (fireCountdown <= 0f)
        {
            Debug.Log("EnemyShoot?");
            Shoot();
            fireCountdown = 1f / fireSpeed;
        }
        fireCountdown -= Time.deltaTime;

    }

    void Shoot()
    {
        GameObject bulletGO = (GameObject)Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);
        EnemyProjectileManager bullet = bulletGO.GetComponent<EnemyProjectileManager>();
        Debug.Log("EnemyDidShoot");
        if (bullet != null)
        {
            bullet.Seek(target);
        }
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, range);
    }


}

