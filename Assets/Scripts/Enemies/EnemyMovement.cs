﻿using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour
{
    public Shop shop;

    public float speed = 10f;
    float originalSpeed;
    public float health;
    public int moneyOnDeath;

    public float fireDamageTaken;
    public float shotDamageTaken;
    public float explosiveDamageTaken;
    public float railGunDamageTaken;

    public GameObject impactEffect;

    private Audio audioPlay;
    private Transform target;
    private Bullet bulletStats;
    private EnemyShootManager enemyShoot;
    private int wavepointIndex = 0;

    public void Start()
    {
        audioPlay = FindObjectOfType<Audio>();
        shop = FindObjectOfType<Shop>();
        originalSpeed = speed;
        enemyShoot = GetComponent<EnemyShootManager>();
        target = Waypoints.points[0];
        bulletStats = GetComponent<Bullet>();
    }

    void Update()
    {
        Vector3 dir = target.position - transform.position;
        transform.Translate(dir.normalized * speed * Time.deltaTime, Space.World);

        if (Vector3.Distance(transform.position, target.position) <= 0.4f)
        {
            GetNextWaypoint();
        }
        if (health <= 0)
        {
            audioPlay.playEnemyDeath();
            shop.totalCurrency += moneyOnDeath;
            Destroy(gameObject);
        }
        if(enemyShoot.inRange == true)
        {
            speed = 0f;
        }
        else
        {
            speed = originalSpeed;
        }

    }

    void GetNextWaypoint()
    {
        if (wavepointIndex >= Waypoints.points.Length - 1)
        {
            Destroy(gameObject);
            return;
        }
        
        wavepointIndex++;
        target = Waypoints.points[wavepointIndex];
        gameObject.transform.LookAt(target);
    }
    public void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "projectile")
        {
            Debug.Log("Hit");
            health -= shotDamageTaken;
            Destroy(other.gameObject);
        }
        if (other.gameObject.tag == "ExplosiveProjectile")
        {
            Explode(other);
            Debug.Log("Oof");
            Destroy(other.gameObject);

        }
        if (other.gameObject.tag == "RailGunBullet")
        {
            health -= railGunDamageTaken;
            Debug.Log("Oof");
            Destroy(other.gameObject);

        }
    }
    public void OnParticleCollision(GameObject other)
    {
        health -= fireDamageTaken;
        Debug.Log("BurnHit");
    }

    void Explode(Collision col)
    {
        Debug.Log("yas");
        Damage(col.transform);

    }

    void Damage(Transform enemy)
    {
        Debug.Log("DamageYES");
        GameObject effectInts = Instantiate(impactEffect, transform.position, transform.rotation);
        Destroy(effectInts, 5f);
        ExplosiveDamage();
    }
    void ExplosiveDamage()
    {
        health -= explosiveDamageTaken;
    }
}

