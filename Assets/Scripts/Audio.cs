﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Audio : MonoBehaviour
{

    private AudioSource audiosource;

    public AudioClip turretDeath;
    public AudioClip turretShoot;
    public AudioClip enemyDeath;

    void Start()
    {
        audiosource = GetComponent<AudioSource>();
    }

    void Update()
    {

    }
    public void playTurretDeath()
    {
        audiosource.clip = turretDeath;
        audiosource.Play();
    }
    public void playTurretShoot()
    {
        audiosource.clip = turretShoot;
        audiosource.Play();

    }
    public void playEnemyDeath()
    {
        audiosource.clip = enemyDeath;
        audiosource.Play();

    }
}
